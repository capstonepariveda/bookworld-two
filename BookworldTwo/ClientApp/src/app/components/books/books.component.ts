import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  private books: any;
  private bookForm: any;

  constructor(private formBuilder: FormBuilder) {
    this.bookForm = this.formBuilder.group({
      title: '',
      author: '',
      pages: Number,
      price: Number,
      published: Date

    })

    
}

  ngOnInit() {
  }

  onSubmit(bookData: any) {
    // Process checkout data here
    console.warn('Your book has been saved', bookData);

    //this.books = this.cartService.clearCart();
    //this.checkoutForm.reset();
  }


}
