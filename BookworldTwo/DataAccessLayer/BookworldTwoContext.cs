﻿using BookworldTwo.Models;
using System;
using System.Collections.Generic;
//using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;
using Microsoft.EntityFrameworkCore;

namespace BookworldTwo.DataAccessLayer
{
    public class BookworldTwoContext: DbContext
    {
      
            public BookworldTwoContext(DbContextOptions<BookworldTwoContext> options) : base(options) //base(BookworldTwoContext)
        {
            
            }

            public DbSet<Book> Books { get; set; }
            public DbSet<Library> Libraries { get; set; }
            public DbSet<Copy> Copies { get; set; }


        //protected override void OnModelCreating(DbModelBuilder modelBuilder)
        //{
        //    modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        //}
    }
}