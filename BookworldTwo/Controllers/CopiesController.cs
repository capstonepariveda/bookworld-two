﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BookworldTwo.DataAccessLayer;
using BookworldTwo.Models;

namespace BookworldTwo.Controllers
{
    [Route("api/copies")]
    [ApiController]
    public class CopiesController : ControllerBase
    {
        private readonly BookworldTwoContext _context;

        public CopiesController(BookworldTwoContext context)
        {
            _context = context;
        }

        // GET: api/Copies
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Copy>>> GetCopies()
        {
            return await _context.Copies.ToListAsync();
        }

        // GET: api/Copies/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Copy>> GetCopy(int id)
        {
            var copy = await _context.Copies.FindAsync(id);

            if (copy == null)
            {
                return NotFound();
            }

            return copy;
        }

        // PUT: api/Copies/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCopy(int id, Copy copy)
        {
            if (id != copy.CopyID)
            {
                return BadRequest();
            }

            _context.Entry(copy).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CopyExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Copies
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Copy>> PostCopy(Copy copy)
        {
            _context.Copies.Add(copy);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetCopy), new { id = copy.CopyID }, copy);
        }

        // DELETE: api/Copies/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Copy>> DeleteCopy(int id)
        {
            var copy = await _context.Copies.FindAsync(id);
            if (copy == null)
            {
                return NotFound();
            }

            _context.Copies.Remove(copy);
            await _context.SaveChangesAsync();

            return copy;
        }

        private bool CopyExists(int id)
        {
            return _context.Copies.Any(e => e.CopyID == id);
        }
    }
}
