﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
namespace BookworldTwo.Models
{
    public enum BookFormat
    {
        Audio, Ebook, Hardback, Paperback
    }
    public class Copy
    {
        [Key] public int CopyID { get; set; }
        public bool CheckedOut { get; set; }
        public BookFormat? BookFormat { get; set; }
        public int BookID { get; set; }
        public Book Book { get; set; }
        public int LibraryID { get; set; }
        public Library Library { get; set; }
    }
}