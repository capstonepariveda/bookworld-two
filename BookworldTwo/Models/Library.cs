﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
namespace BookworldTwo.Models
{
    public class Library
    {
        [Key] public int LibraryID { get; set; }
        public string LibraryName { get; set; }
        public string Manager { get; set; }
        public ICollection<Copy> Copies { get; set; }
    }
}